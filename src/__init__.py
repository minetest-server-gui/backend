# Minetest Web Gui is a webapp for managing a minetest server via a gui
# Copyright (C) 2020  SonoMichele (Michele Viotto)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from flask import Flask
from flask_cors import CORS
from shelljob import proc
from pony.flask import Pony
from pony.orm import Database
from flask_bcrypt import Bcrypt

from src.config import Config


db = Database()
bcrypt = Bcrypt()
SERVER_GROUP = proc.Group()
SERVER_PROCESS = None


def create_app(config_class=Config):
    app = Flask(__name__)
    cors = CORS(app, resources={r"/*": {"origins": "*"}})
    app.config.from_object(config_class)

    db.bind(provider='sqlite', filename='db.sqlite', create_db=True)
    from src.models import User, Permission
    db.generate_mapping(create_tables=True)

    from src.views import main, console, auth
    app.register_blueprint(main.main)
    app.register_blueprint(console.console, url_prefix='/console')
    app.register_blueprint(auth.auth, url_prefix='/auth')

    Pony(app)
    bcrypt.init_app(app)

    return app
