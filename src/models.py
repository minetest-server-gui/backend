# Minetest Web Gui is a webapp for managing a minetest server via a gui
# Copyright (C) 2020  SonoMichele (Michele Viotto)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pony.orm import PrimaryKey, Required, Optional, Set

from src import db, bcrypt


class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    username = Required(str, unique=True)
    password = Required(str, 60)
    email = Optional(str, unique=True)
    permissions = Set('Permission')

    @staticmethod
    def authenticate(**kwargs):
        username = kwargs.get('username')
        password = kwargs.get('password')

        if not username or not password:
            return None

        user = User.get(username=username)
        if not user or not bcrypt.check_password_hash(user.password, password):
            return None

        return user


class Permission(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str, unique=True)
    description = Required(str)
    user = Set('User')
