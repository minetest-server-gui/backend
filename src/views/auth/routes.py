# Minetest Web Gui is a webapp for managing a minetest server via a gui
# Copyright (C) 2020  SonoMichele (Michele Viotto)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime, timedelta

from flask import Blueprint, jsonify, request, current_app
import jwt

from src.models import User
from src.util import token_required
from src import bcrypt


auth = Blueprint('auth', __name__)


@auth.route('/register/', methods=['POST'])
def register():
    data = request.get_json()
    username = data['username']
    password = data['password']
    user = User(username=username, password=bcrypt.generate_password_hash(password).decode('utf-8'))

    return jsonify(user.to_dict()), 201


@auth.route('/login/', methods=['POST'])
def login():
    data = request.get_json()
    user = User.authenticate(**data)

    if not user:
        return jsonify({'message': 'Invalid credentials', 'authenticated': False}), 401
    
    token = jwt.encode({
        'sub': user.username,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(minutes=30)},
        current_app.config['SECRET_KEY'],
        algorithm="HS256"
    )
    
    return jsonify({'token': token})


@auth.route('/test/')
@token_required
def test(current_user):
    return jsonify(current_user.to_dict()), 201
