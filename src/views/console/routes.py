# Minetest Web Gui is a webapp for managing a minetest server via a gui
# Copyright (C) 2020  SonoMichele (Michele Viotto)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import signal

from flask import Blueprint, Response, flash, jsonify
from flask_cors import cross_origin

from src import SERVER_GROUP, SERVER_PROCESS


console = Blueprint('console', __name__)


# this thing streams the console output and is used by the index route
@console.route('/stream')
def stream():
    def read_process():
        global SERVER_GROUP
        global SERVER_PROCESS
        while SERVER_GROUP.is_pending():
            lines = SERVER_GROUP.readlines()
            for _, line in lines:
                #if '[24d]' not in line:  # the stream spams lines starting with this pattern, so I exclude it and the user doesn't see it
                yield 'data:' + line + '\n\n'
            # _, line = SERVER_GROUP.readline()
            # print(line.decode('utf-8'))
            # yield 'data: ' + line.decode('utf-8')

    return Response(read_process(), mimetype='text/event-stream')


@console.route('/start')
def start():
    global SERVER_PROCESS
    if SERVER_PROCESS is None:
        # i need encoding=utf-8 or the input doesn't work and if I use shell=True the command ignores parameters idk why
        SERVER_PROCESS = SERVER_GROUP.run(["minetestserver --logfile log.txt --port 30000 --worldname world"], encoding='utf-8', shell=True)
        # SERVER_PROCESS = subprocess.Popen('minetestserver --terminal', text=True, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return {
            'status': 'started',
            'pid': SERVER_PROCESS.pid,
            'category': 'alert-success',
            'msg': f'The server has been started and is running with PID {SERVER_PROCESS.pid}'
        }
    return {
            'status': 'already_running',
            'pid': SERVER_PROCESS.pid,
            'category': 'alert-warning',
            'msg': f'The server is already running with PID {SERVER_PROCESS.pid}'
        }


@console.route('/stop')
def stop():
    global SERVER_PROCESS
    if SERVER_PROCESS is not None:
        SERVER_PROCESS.send_signal(signal.SIGTERM)
        SERVER_PROCESS = None
        return {
            'status': 'stopped',
            'category': 'alert-success',
            'msg': f'The server has been stopped'
        }
    return {
            'status': 'already_stopped',
            'category': 'alert-warning',
            'msg': f'The server is not running'
        }


@console.route('/settings')
def settings():
    settings_dict = [
        {
            'type': "text",
            'labelText': "Server Name",
            'settingName': "server_name",
            'defaultValue': "Minetest Server",
            'actualValue': "[Minigames] A.E.S."
        },
        {
            'type': "textarea",
            'labelText': "Server Description",
            'settingName': "server_description",
            'defaultValue': "mine here",
            'actualValue': ""
        },
        {
            'type': "text",
            'labelText': "Server Address",
            'settingName': "server_address",
            'defaultValue': "game.minetest.net",
            'actualValue': ""
        },
        {
            'type': "int",
            'labelText': "Server Port",
            'settingName': "port",
            'defaultValue': "30000",
            'actualValue': ""
        },
        {
            'type': "bool",
            'labelText': "Server Announce",
            'settingName': "server_announce",
            'defaultValue': False,
            'actualValue': False
        }
    ]

    return jsonify(settings_dict)
